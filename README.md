# Docker images with AWS CLI for GitLab CI on Fargate

Docker images ready to run AWS CLI-based CI jobs with the [AWS Fargate Custom
Executor](https://gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/fargate)
driver for [GitLab Runner](https://docs.gitlab.com/runner).

[![pipeline
status](https://gitlab.com/aws-fargate-driver-demo/docker-aws-cli-gitlab-ci-fargate/badges/master/pipeline.svg)](https://gitlab.com/aws-fargate-driver-demo/docker-aws-cli-gitlab-ci-fargate/-/commits/master)

## AWS-related environment variables handling

### Persistent Amazon ECS security credentials

[Amazon ECS temporary/unique security
credentials](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task-iam-roles.html)
are fetched and stored into the `~/.aws/credentials` file when a container is
started.

- [ubuntu/aws-credentials](./ubuntu/aws-credentials)

Pros:

1. the temporary security credentials are loaded only once for each container
   (when it is started).

Cons:

1. persists only the Amazon ECS temporary/unique security credentials, which
   means other AWS-related environment variables are lost in subsequent login
   sessions;
1. currently allows only one container user to leverage the temporary security
   credentials.

### Cross-session environment variables

Environment variables are automatically filtered and propagated to all
interactive login sessions, including the ones started over ssh.

**Non-interactive sessions, such as the ones created by the AWS Fargate Custom
Executor driver, must explicitly source the `/etc/profile.d/set-aws-env-vars.sh`
file (created by the entry point script) before using any `aws` commands for the
first time in a given CI job**.

- [ubuntu/etc-profile.d](./ubuntu/etc-profile.d)

Pros:

1. allows **all container users** to access **all AWS-related environment
   variables**, not only the temporary Amazon ECS security credentials.

Cons:

1. may need to be customized to work in other Linux distributions
   (tested only with Ubuntu until now);
1. requires running `. /etc/profile.d/set-aws-env-vars.sh` before calling `aws`
   commands for the first time in a given CI job.
