#!/bin/sh

propagateAWSEnvVarsAllLoginSessions() {

  # Store all AWS-related environment variables into a list.
  AWS_ENV_VARS=$(printenv | grep 'AWS_\|ECS_')

  # Create a script to export the AWS-related environment variables.
  SET_AWS_ENV_VARS_SCRIPT=/etc/profile.d/set-aws-env-vars.sh
  touch $SET_AWS_ENV_VARS_SCRIPT

  # Start the script from scratch.
  echo '' > $SET_AWS_ENV_VARS_SCRIPT

  # Write the `export environment variable` commands into the script.
  for VARIABLE in $AWS_ENV_VARS
  do
    echo "export $VARIABLE" >> $SET_AWS_ENV_VARS_SCRIPT
  done
}

setUpSSH() {

  # Block the container to start without an SSH public key. 
  if [ -z "$SSH_PUBLIC_KEY" ]; then
    echo 'Need your SSH public key as the SSH_PUBLIC_KEY environment variable.'
    exit 1
  fi

  # Create a folder to store user's SSH keys if it does not exist.
  USER_SSH_KEYS_FOLDER=~/.ssh
  [ ! -d "$USER_SSH_KEYS_FOLDER" ] && mkdir -p $USER_SSH_KEYS_FOLDER

  # Copy contents from the `SSH_PUBLIC_KEY` environment variable
  # to the `${USER_SSH_KEYS_FOLDER}/authorized_keys` file.
  echo $SSH_PUBLIC_KEY > ${USER_SSH_KEYS_FOLDER}/authorized_keys

  # Clear the `SSH_PUBLIC_KEY` environment variable.
  unset SSH_PUBLIC_KEY

  # Start the SSH daemon.
  /usr/sbin/sshd -D
}

propagateAWSEnvVarsAllLoginSessions

setUpSSH
